#!/usr/bin/env bash
rsync -a project/ project-test/
my_input='old1 new1 name1
000 fff refs/heads/master
'
echo "$my_input" | ./project-test/hooks/pre-receive
./project-test/hooks/update ref_name old_rev new_rev
echo "$my_input" | ./project-test/hooks/post-receive
