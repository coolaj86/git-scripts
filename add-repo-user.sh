#!/usr/bin/env bash
set -e
set -u

read -p "Repository Username [www-repos]: " my_user
#${my_user:=www-repos}
my_user=${my_user:-"www-repos"}

read -p "Repository Directory [/srv/$my_user]: " my_repos
my_repos=${my_repos:-"/srv/$my_user"}

read -p "Deploy Directory [/srv/www]: " my_www
my_www=${my_www:-"/srv/www"}

echo ""

set -x
# user
sudo adduser --disabled-password --gecos "Git Site Deploys" --home "$my_repos" --no-create-home $my_user >/dev/null

# ssh
sudo mkdir -p "$my_repos/.ssh" "$my_www"
sudo chmod 0700 "$my_repos/.ssh"
sudo ssh-keygen -t rsa -N "" -C "$my_user@$(hostname)" -f "$my_repos/.ssh/id_rsa"
sudo install -m 0600 /dev/null "$my_repos/.ssh/authorized_keys"

# git templates
sudo rsync -a git-templates/project/ "$my_repos/git-template/"
sudo install -m 0644 /dev/null "$my_repos/.gitconfig"
my_config='[init]
  templatedir = '"$my_repos/git-template"
sudo bash -c "echo '$my_config' >> '$my_repos/.gitconfig'"

# fix permissions
sudo chown -R $my_user:$my_user "$my_repos" "$my_www"
set +x

echo ""
echo ""
echo "To enable git deploys from this account you will need to add its public key to your git deployer:"
echo ""
echo "$my_repos/.ssh/id_rsa.pub:"
echo ""
sudo cat "$my_repos/.ssh/id_rsa.pub"
echo ""
