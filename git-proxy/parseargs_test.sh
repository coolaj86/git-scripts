# usage
source ./parseargs.sh
MY_ARGS="foo 'bar baz' qux * "'$(dangerous)'" sudo ls -lah"
IFS=$'\r\n' GLOBIGNORE='*' args=($(parseargs "$MY_ARGS"))
echo '$@': "${args[@]}"
for arg in "${args[@]}"; do
	echo "$arg"
done
