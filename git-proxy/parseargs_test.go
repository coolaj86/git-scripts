package main

import (
	"fmt"
	"log"
	"strings"
	"testing"
)

func TestStrings(t *testing.T) {
	tests := [][]string{
		[]string{
			`  a   'b'   ''   '"d" e' " f " ""''""''''"""" g"h"'i'jkl'mno'pqr $("dangerous dangerous danger-ous-ous-ous")`,
			"a", "b", "", `"d" e`, " f ", "", "ghijklmnopqr", "$(dangerous dangerous danger-ous-ous-ous)",
		},
		[]string{
			` arg1 arg2 '   hello    world'"'" '' " '  another hello world   ' "  `,
			"arg1", "arg2", "   hello    world'", "", " '  another hello world   ' ",
		},
		[]string{
			`arg1 arg2 '   hello    world'"'" " "" '  another hello world   ' "`,
			"arg1", "arg2", "   hello    world'", "  '  another hello world   ' ",
		},
		[]string{
			` arg1 arg2 '   hello    world'"'" "" " '  another hello world   ' "`,
			"arg1", "arg2", "   hello    world'", "", " '  another hello world   ' ",
		},
		[]string{
			`arg1 arg2 '   hello    world'"'" "" " '  another hello world   ' `,
		},
	}

	for i := range tests {
		strs := tests[i]
		in := strs[0]
		expected := strs[1:]

		actual, _ := ParseArgs(in)
		if strings.Join(actual, "#") != strings.Join(expected, "#") {
			fmt.Printf("Expected: %#v\n", expected)
			fmt.Printf("Actual: %#v\n", actual)
			log.Fatal("Test failed.")
		}
	}
}
