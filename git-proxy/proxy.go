package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var ErrGitOnlyShell = errors.New("You've successfully authenticated, but this is a git-only shell")
var gitcmds = map[string]bool{
	"git-receive-pack":   true,
	"git-upload-pack":    true,
	"git-upload-archive": true,
}

func main() {
	cmds, err := ParseArgs(os.Getenv("SSH_ORIGINAL_COMMAND"))
	if nil != err {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	if len(cmds) < 2 {
		fmt.Fprintf(os.Stderr, "%s\n", ErrGitOnlyShell)
		os.Exit(1)
	}

	bin := cmds[0]
	_, ok := gitcmds[bin]
	if !ok {
		fmt.Fprintf(os.Stderr, "%s\n", ErrGitOnlyShell)
		os.Exit(1)
	}

	args := cmds[1:]
	cmd := exec.Command(bin, args...)
	cmd.Env = append(os.Environ(), "GIT_PROXY=true")
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
