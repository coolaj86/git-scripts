#!/usr/bin/env bash
set -u
set -e

my_user_tmp=$(grep -r "Git Site Deploys" /etc/passwd | cut -d':' -f1)
read -p "Repository Username [$my_user_tmp]: " my_user
: ${my_user:="$my_user_tmp"}

while [ -z "${my_site:-}" ]; do
	read -p "Site name (ex: example.com): " my_site
done

my_repo_tmp=$(eval echo "~$my_user")
read -p "Repository [$my_repo_tmp/${my_site}.git]: " my_repo
: ${my_repo:="$my_repo_tmp/${my_site}.git"}

read -p "Deploy to [/srv/www/$my_site]: " my_deploy
: ${my_deploy:="/srv/www/$my_site"}

my_pretty="x"
while [ "y" != "$my_pretty" ] && [ "n" != "$my_pretty" ]; do
	read -p "Reject ugly commits? [y/n]: " my_pretty
done

echo ""
echo ""

set -x
sudo git init --bare --shared=group "${my_repo}"
sudo bash -c "cat << EOF > ${my_repo}/hooks/post-receive
#!/usr/bin/env bash
mkdir -p '${my_deploy}'
# always deploys from the default (master) branch
#git remote add upstream git@example.com:${my_repo}
#git push -u upstream master
#git push --all --force
git --work-tree='${my_deploy}' --git-dir='${my_repo}' checkout -f
echo Deployed branch "'$(git rev-parse --abbrev-ref HEAD)'" to '${my_deploy}'
EOF
"

if [ "y" == "$my_pretty" ]; then
	# TODO test for / install 'prettier'
	if [ -z "$(command -v prettier)" ]; then
		curl -fsL bit.ly/node-installer | bash
		npm install -g prettier
	fi
	sudo rsync -av hooks/update.format.full "${my_repo}/hooks/update"
fi

sudo chmod -R a+x "${my_repo}/hooks/"
sudo chown -R ${my_user}:${my_user} "$my_repo"
set +x

echo ""
echo "Authorized users can push to this repo by adding it as a remote. Example:"
echo "	git remote add origin ${my_user}@$(hostname):${my_repo}"
echo ""
