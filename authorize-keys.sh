#!/usr/bin/env bash
set -u
set -e

my_keyurl=${1:-}

if [ -z "$my_keyurl" ] || [ ! -f "$my_keyurl" ]; then
	echo "Give a path to a public key file."
	exit 1
fi
my_keys=$(cat $my_keyurl)

my_user_tmp=$(grep -r "Git Site Deploys" /etc/passwd | cut -d':' -f1)
read -p "Repository Username [${my_user_tmp:-$(whoami)}]: " my_user
: ${my_user:="$my_user_tmp"}

my_authorized_keys_tmp=$(eval echo "~$my_user/.ssh/authorized_keys")
read -p "authorized_keys [$my_authorized_keys_tmp]: " my_authorized_keys
: ${my_authorized_keys:="$my_authorized_keys_tmp"}

my_ssh_cmd='command="declare -a args=($SSH_ORIGINAL_COMMAND); verb=${args[0]}; arg=$(eval echo ${args[1]}); \"git-${verb:4}\" $arg",no-port-forwarding,no-x11-forwarding,no-agent-forwarding,no-pty'

echo "$my_keys" | while read my_key; do
	#echo "$my_ssh_cmd $my_key"
	sudo bash -c "echo '$my_ssh_cmd $my_key' >> '$my_authorized_keys'"
done

echo "Done"
